// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from './widgets';
import appService from './task-service';

class App extends Component {
  code: string = '';
  stOutput: string = '';
  stError: string = '';
  stExit: number = 0;

  render() {
    return (
      <>
        <Card>
          App.js
          <Row>
            <Column>
              <Form.Textarea
                type="text"
                value={this.code}
                onChange={(event) => (this.code = event.currentTarget.value)}
                rows={10}
              />
            </Column>
          </Row>
          <Button.Success
            onClick={() =>
              appService.send(this.code).then((response) => {
                (this.stOutput = response.data[0]),
                  (this.stError = response.data[1]),
                  (this.stExit = response.data[2]);
                console.log(this.stExit);
              })
            }
          >
            Run
          </Button.Success>
        </Card>
        <Card>
          Standard Output:
          <Form.Label>{this.stOutput}</Form.Label>
        </Card>
        <Card>
          Standard Error:
          <Form.Label>{this.stError}</Form.Label>
        </Card>
        <Card>
          Standard Exit Status:
          <Form.Label>{this.stExit}</Form.Label>
        </Card>
      </>
    );
  }

  mounted() {}

  // func() {
  //   appService.send(this.code).then((response) => {
  //     (this.stOutput = response.data[0]),
  //       (this.stError = response.data[1]),
  //       (this.stExit = response.data[2]);
  //   });
  // }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <App />
    </>,
    root
  );
