// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

class AppService {
  send(code: string) {
    return axios.post<{}, { code: string }>('/', { code: code });
    /*     .then((response) => {
        response.data;
      });*/
  }
}

const appService = new AppService();
export default appService;
