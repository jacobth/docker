// @flow
import express from 'express';
import appService from './task-service';
const { spawn } = require('child_process');

/**
 * Express router containing task methods.
 */
const router: express$Router<> = express.Router();

router.post('/', (request, response) => {
  //console.log('hei2');
  let stOut: string = '';
  let stErr: string = '';
  let stExit: number = 0;
  let nice: string = request.body.code;
  // console.log(nice);
  const docker = spawn('docker', ['run', '--rm', 'node-image', 'node', '-e', nice]);
  console.log(docker);
  docker.stdout.on('data', (data) => {
    stOut += data;
  });

  docker.stderr.on('data', (data) => {
    stErr += data;
  });

  docker.on('close', (code) => {
    stExit += code;
    console.log([String(stOut), stErr, stExit]);
    response.send([stOut, stErr, stExit]);
  });
});

export default router;
